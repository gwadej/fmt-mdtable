//! Struct representing the alignment and size of a column.
//!
//! # Examples
//!
//! ```rust
//! use mdtable::Alignment;
//!
//! # fn main() {
//! let align = Alignment::default();
//! assert!(align == Alignment::NoAlign(3.into()));
//!
//! let align = Alignment::new(":----", 10);
//! assert!(Alignment::LeftAlign(10.into()) == align);
//! # }
//! ```
//!
//! # Description
//!
//! Describes the size and alignment of a column.

use std::cmp;

pub mod width;

pub use width::ColWidth;

/// Variants cover different styles of alignment and the width of a column
#[derive(Debug, Eq, PartialEq)]
pub enum Alignment {
    /// No alignment specified, defaulting to left
    NoAlign(ColWidth),
    /// Left justified
    LeftAlign(ColWidth),
    /// Centered
    CenterAlign(ColWidth),
    /// Right justified
    RightAlign(ColWidth)
}

/// Default column is non-aligned, with a width of 3
impl Default for Alignment {
    fn default() -> Self { Self::NoAlign(ColWidth::default()) }
}

impl Alignment {
    /// Create a new Alignment, based on the supplied separator column and length
    ///
    /// Code assumes that only '-' and ':' are part of the `sep_col`. Treats any non-':' as a '-' for
    /// purposes of defining alignment.
    #[must_use]
    pub fn new(sep_col: &str, len: usize) -> Self {
        let width = ColWidth::new(len);

        if sep_col == ":" { return Self::LeftAlign(width); }
        if sep_col == "-" { return Self::NoAlign(width); }

        match (sep_col.starts_with(':'), sep_col.ends_with(':')) {
            (true, false) => Self::LeftAlign(width),
            (true, true) => Self::CenterAlign(width),
            (false, true) => Self::RightAlign(width),
            (false, false) => Self::NoAlign(width),
        }
    }

    /// Updates the width of the Alignment, if the supplied length is larger than the current.
    pub fn update_len(&mut self, len: usize) {
        let len = ColWidth::new(len);
        match self {
            Self::NoAlign(width) |
            Self::LeftAlign(width) |
            Self::CenterAlign(width) |
            Self::RightAlign(width) => *width = cmp::max(*width, len),
        }
    }

    // Do the actual work of centering the supplied column.
    fn center_column(width: ColWidth, col: &str) -> String {
        if width.get() <= col.len() { return col.to_string(); }

        let diff = width.get() - col.len();
        let left = diff / 2;
        let right = diff - left;
        format!("{0:<left$}{col}{0:>right$}", "")
    }

    /// Format a column with the aligment defined by the variant.
    #[must_use]
    pub fn format_col(&self, col: &str) -> String {
        match self {
            Self::NoAlign(width) |
                Self::LeftAlign(width) => format!("{col:<width$}", width = width.get()),
            Self::CenterAlign(width) => Self::center_column(*width, col),
            Self::RightAlign(width) => format!("{col:>width$}", width = width.get()),
        }
    }

    /// Create a string representing the separator for the column in markdown.
    #[must_use]
    pub fn separator(&self) -> String {
        match self {
            Self::NoAlign(width) => format!("{:-<width$}", '-', width = width.get()),
            Self::LeftAlign(width) => format!("{:-<width$}", ':', width = width.get()),
            Self::CenterAlign(width) => format!(":{:->width$}", ':', width = width.get() - 1),
            Self::RightAlign(width) => format!("{:->width$}", ':', width = width.get()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use lets_expect::lets_expect;

    lets_expect! {
        let default_align = Alignment::NoAlign(3.into());
        // A few generalized creation tests for special cases.
        expect(Alignment::default()) to equal(default_align)
        expect(Alignment::new("----", 2)) as undersized to equal(default_align)
        expect(Alignment::new(":", 5)) as single_colon to equal(Alignment::LeftAlign(5.into()))
        expect(Alignment::new("-", 5)) as single_dash to equal(Alignment::NoAlign(5.into()))

        when(alignment = Alignment::NoAlign(len.into())) as no_align {
            expect(Alignment::new("----", 5)) as interpret_sep {
                let len = 5;
                to equal(alignment)
            }
            when(len = 10) as update_len {
                let mut align = Alignment::NoAlign(4.into());
                expect(&align) to make({ align.update_len(len); align }) equal(alignment)

                when attempt_shrink {
                    let mut align = Alignment::NoAlign(len.into());
                    expect(&align) to make({ align.update_len(4); align }) equal(alignment)
                }
            }
            when(len = 10, expected = String::from("Mark      ")) as short_string
                expect(alignment) to have(format_col("Mark")) equal(expected)
            when(len = 5, expected = String::from("Long string")) as long_string
                expect(alignment) to have(format_col("Long string")) equal(expected)

            when(len = 6, expected = String::from("------"))
                expect(alignment) to have(separator()) equal(expected)

        }

        when(alignment = Alignment::LeftAlign(len.into())) as left_align {
            expect(Alignment::new(":---------", 4)) as interpret_sep {
                let len = 4;
                to equal(alignment)
            }
            when(len = 10) as update_len {
                let mut align = Alignment::LeftAlign(4.into());
                expect(&align) to make({ align.update_len(len); align }) equal(alignment)

                when attempt_shrink {
                    let mut align = Alignment::LeftAlign(len.into());
                    expect(&align) to make({ align.update_len(4); align }) equal(alignment)
                }
            }
            when(len = 10, expected = String::from("Mark      ")) as short_string
                expect(alignment) to have(format_col("Mark")) equal(expected)
            when(len = 5, expected = String::from("Long string")) as long_string
                expect(alignment) to have(format_col("Long string")) equal(expected)

            when(len = 6, expected = String::from(":-----"))
                expect(alignment) to have(separator()) equal(expected)

        }

        when(alignment = Alignment::CenterAlign(len.into())) as center_align {
            expect(Alignment::new(":-:", 6)) as interpret_sep {
                let len = 6;
                to equal(alignment)
            }
            when(len = 10) as update_len {
                let mut align = Alignment::CenterAlign(4.into());
                expect(&align) to make({ align.update_len(len); align }) equal(alignment)

                when attempt_shrink {
                    let mut align = Alignment::CenterAlign(len.into());
                    expect(&align) to make({ align.update_len(4); align }) equal(alignment)
                }
            }
            when(len = 10, expected = String::from("   Mark   ")) as short_string
                expect(alignment) to have(format_col("Mark")) equal(expected)
            when(len = 11, expected = String::from("   Mark    ")) as short_string_uneven
                expect(alignment) to have(format_col("Mark")) equal(expected)
            when(len = 5, expected = String::from("Long string")) as long_string
                expect(alignment) to have(format_col("Long string")) equal(expected)

            when(len = 6, expected = String::from(":----:"))
                expect(alignment) to have(separator()) equal(expected)

        }

        when(alignment = Alignment::RightAlign(len.into())) as right_align {
            expect(Alignment::new("-----:", 4)) as interpret_sep {
                let len = 4;
                to equal(alignment)
            }
            when(len = 10) as update_len {
                let mut align = Alignment::RightAlign(4.into());
                expect(&align) to make({ align.update_len(len); align }) equal(alignment)

                when attempt_shrink {
                    let mut align = Alignment::RightAlign(len.into());
                    expect(&align) to make({ align.update_len(4); align }) equal(alignment)
                }
            }
            when(len = 10, expected = String::from("      Mark")) as short_string
                expect(alignment) to have(format_col("Mark")) equal(expected)
            when(len = 5, expected = String::from("Long string")) as long_string
                expect(alignment) to have(format_col("Long string")) equal(expected)

            when(len = 6, expected = String::from("-----:"))
                expect(alignment) to have(separator()) equal(expected)

        }
    }
}
