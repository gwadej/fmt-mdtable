//! Struct representing a row in the table
//!
//! # Description
//!
//! Represent the cells of a row
use std::fmt;
use std::iter;
use std::str::FromStr;

use crate::Alignment;

/// A row is a vector of strings that represent the data in the cells in the row.
#[derive(Debug, Default)]
pub struct Row(Vec<String>);

/// Combine a [`Row`] and an array slice of [`Alignment`] items to use to format the row.
#[allow(clippy::module_name_repetitions)]
pub struct AlignedRow<'a>(&'a Row, &'a [Alignment]);

// Represent an iterator over the cells in a row.
type Iter<'a> = std::slice::Iter<'a, String>;

impl Row {
    /// Create a new [`Row`]
    #[must_use]
    pub fn new(cells: Vec<String>) -> Self { Self(cells) }

    /// Return the number of cells in the [`Row`].
    #[must_use]
    pub fn len(&self) -> usize { self.0.len() }

    /// Return `true` if the [`Row`] has no cells.
    #[must_use]
    pub fn is_empty(&self) -> bool { self.0.is_empty() }

    /// Return an iterator over the cells in the [`Row`].
    pub fn iter(&self) -> Iter {
        self.0.iter()
    }

    /// Create an [`AlignedRow`] given an array slice of [`Alignment`]s
    #[must_use]
    pub const fn align<'a>(&'a self, align: &'a [Alignment]) -> AlignedRow<'a> {
        AlignedRow(self, align)
    }
}

impl<'a> IntoIterator for &'a Row {
    type IntoIter = std::slice::Iter<'a, std::string::String>;
    type Item = &'a std::string::String;
    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a> fmt::Display for AlignedRow<'a> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "| {} |",
            self.0.iter().chain(iter::repeat(&String::new()))
                .zip(self.1.iter())
                .map(|(c,s)| s.format_col(c)).collect::<Vec<String>>()
                .join(" | ")
        )
    }
}

impl fmt::Display for Row {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "| {} |", self.0.join(" | "))
    }
}

// This needs work. Only happy path at the moment.
impl FromStr for Row {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut raw_cols: Vec<String> = s
            .split('|').map(|s| String::from(s.trim())).skip(1).collect();
        raw_cols.pop();
        Ok(Self(raw_cols))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use lets_expect::lets_expect;

    lets_expect! {
        let row = Row::new(cells);
        expect(row) {
            when(cells = vec![
                    String::from("a"),
                    String::from("abc"),
                    String::from("def")
                ]) as new {
                let expected = vec![ "a", "abc", "def" ];
                to have(len()) equal(expected.len())

                let cell_labels = row.iter().map(String::as_str).collect::<Vec<_>>();
                expect(cell_labels) to equal(expected)
            }

            when(cells = vec![
                String::from("abc"),
                String::from("def"),
                String::from("ghi"),
                String::from("jkl"),
                String::from("mno")
            ]) as formatting {
                let expected = String::from("| abc | def | ghi | jkl | mno |");
                to have(to_string()) equal(expected)

                when aligned {
                    let aligns = vec![
                        Alignment::NoAlign(5.into()),
                        Alignment::LeftAlign(4.into()),
                        Alignment::RightAlign(5.into()),
                        Alignment::CenterAlign(7.into()),
                        Alignment::LeftAlign(4.into()),
                    ];
                    let expected = String::from("| abc   | def  |   ghi |   jkl   | mno  |");
                    let aligned_row = row.align(&aligns);
                    expect(aligned_row.to_string()) to equal(expected)
                }
            }
        }
    }
}
