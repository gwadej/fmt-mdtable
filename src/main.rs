use std::io;
use anyhow::Result;

use mdtable::table::{AddStatus, TableParser};

fn main() -> Result<()> {
    let stdin = io::stdin();
    let lines = stdin.lines();

    let mut parser = TableParser::default();

    for line in lines {
        let line = line?;
        match parser.add_line(&line)? {
            AddStatus::NotStarted => println!("{line}"),
            AddStatus::Finish => {
                println!("{}", parser.finish()?);
                parser = TableParser::default();
                println!("{line}");
            }
            AddStatus::Added => (),
        }
    }

    println!("{}", parser.finish()?);

    Ok(())
}
