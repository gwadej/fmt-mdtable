//! Representation of a markdown table
//!
//! # Examples
//!
//!```rust
//! use mdtable::Table;
//!
//! let table: Table = table_str.parse().unwrap();
//! println!("{}", table);
//!```
//!
//! # Description
//!
//! Represents a markdown table. Main functionality is aligning the columns and formatting for
//! output.

use std::fmt;
use std::str;

use crate::{Alignment, Row};

mod parser;

#[allow(clippy::module_name_repetitions)]
pub use parser::{AddStatus, Error, TableParser};

/// Represent the markdown table.
#[derive(Debug, Default)]
pub struct Table {
    heading: Row,
    col_defs: Vec<Alignment>,
    rows: Vec<Row>,
    indent: usize
}

/// Parse a table out of the supplied string.
impl str::FromStr for Table {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parser = TableParser::default();
        parser.parse_str(s)
    }
}

impl Table {
    // Extend the alignment Vec if the supplied [`Row`] is longer
    fn extend_columns(&mut self, row: &Row) {
        if self.col_defs.len() < row.len() {
            for col in row.iter().skip(self.col_defs.len()) {
                let mut desc = Alignment::default();
                desc.update_len(col.len());

                self.col_defs.push(desc);
            }
        }
    }

    // Add a heading row to the table
    fn add_heading(&mut self, cells: Row, indent: usize) {
        self.heading = cells;
        self.indent = indent;
    }

    // Add alignment definitions to the table
    fn add_separator(&mut self, align: Vec<Alignment>) {
        self.col_defs = align;
        for (a, l) in self.col_defs.iter_mut().zip(self.heading.iter().map(String::len)) {
            a.update_len(l);
        }
    }

    // Add a row to the table
    fn add_row(&mut self, row: Row) {
        self.col_defs.iter_mut()
            .zip(row.iter())
            .for_each(|(sep, col)| sep.update_len(col.len()));
        self.extend_columns(&row);

        self.rows.push(row);
    }
}

impl fmt::Display for Table {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let indent = format!("{:indent$}", "", indent = self.indent);

        // heading
        writeln!(f, "{indent}{}", self.heading.align(&self.col_defs))?;
        // separator line
        writeln!(f, "{indent}| {} |",
            self.col_defs.iter().map(Alignment::separator).collect::<Vec<String>>().join(" | "),
        )?;
        // rows
        for row in &self.rows {
            writeln!(f, "{indent}{}", row.align(&self.col_defs))?;
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use lets_expect::lets_expect;

    lets_expect! {
        when(result: Result<Table, Error> = content.parse()) as parse_error {
            when(content = 
r"This is not a table

| Name | Date | Notes |
| --- | --- | --- |
| David | 1982-01-02 | Electrical powers, comic fan |
| Kirsten | 1985-02-01 | Super-strength, bullet-proof, short |
| Mark | ? | Dragon |
| Connie | 1983-03-03 | Psychic sword, Olympic fencer |
") as content_before_table
            expect(result) to be_err_and equal(Error::TableNotFound)

            when(content = 
r"| Name | Date | Notes |
| David | 1982-01-02 | Electrical powers, comic fan |
| Kirsten | 1985-02-01 | Super-strength, bullet-proof, short |
| Mark | ? | Dragon |
| Connie | 1983-03-03 | Psychic sword, Olympic fencer |
") as missing_separator_table
            expect(result) to be_err_and equal(Error::MissingSeparator)

            when(content = 
r"| Name | Date | Notes |
") as header_only_table
            expect(result) to be_err_and equal(Error::MissingSeparator)

            when(content = 
r"
| Name | Date | Notes |
| --- | --- | --- |
| David | 1982-01-02 | Electrical powers, comic fan |
| Kirsten | 1985-02-01 | Super-strength, bullet-proof, short |
| Mark | ? | Dragon |
| Connie | 1983-03-03 | Psychic sword, Olympic fencer |
Something else is here.
") as content_after_table
            expect(result) to be_err_and equal(Error::ContentAfterTable)
        }

        when(table: Table = content.parse().unwrap()) as parse {
            when simple_table {
                let expected = String::from(
r"| Name    | Date       | Notes                               |
| ------- | ---------- | ----------------------------------- |
| David   | 1982-01-02 | Electrical powers, comic fan        |
| Kirsten | 1985-02-01 | Super-strength, bullet-proof, short |
| Mark    | ?          | Dragon                              |
| Connie  | 1983-03-03 | Psychic sword, Olympic fencer       |
");
                let content = 
r"| Name | Date | Notes |
| --- | --- | --- |
| David | 1982-01-02 | Electrical powers, comic fan |
| Kirsten | 1985-02-01 | Super-strength, bullet-proof, short |
| Mark | ? | Dragon |
| Connie | 1983-03-03 | Psychic sword, Olympic fencer |
";
                expect(&table) to make(format!("{}", table)) equal(expected)
            }

            when indent_1_table {
                let expected = String::from(
r" | Name    | Date       | Notes                               |
 | ------- | ---------- | ----------------------------------- |
 | David   | 1982-01-02 | Electrical powers, comic fan        |
 | Kirsten | 1985-02-01 | Super-strength, bullet-proof, short |
 | Mark    | ?          | Dragon                              |
 | Connie  | 1983-03-03 | Psychic sword, Olympic fencer       |
");
                let content = 
r" | Name | Date | Notes |
 | --- | --- | --- |
 | David | 1982-01-02 | Electrical powers, comic fan |
 | Kirsten | 1985-02-01 | Super-strength, bullet-proof, short |
 | Mark | ? | Dragon |
 | Connie | 1983-03-03 | Psychic sword, Olympic fencer |
";
                expect(&table) to make(format!("{}", table)) equal(expected)
            }

            when indent_2_table {
                let expected = String::from(
r"  | Name    | Date       | Notes                               |
  | ------- | ---------- | ----------------------------------- |
  | David   | 1982-01-02 | Electrical powers, comic fan        |
  | Kirsten | 1985-02-01 | Super-strength, bullet-proof, short |
  | Mark    | ?          | Dragon                              |
  | Connie  | 1983-03-03 | Psychic sword, Olympic fencer       |
");
                let content = 
r"  | Name | Date | Notes |
  | --- | --- | --- |
  | David | 1982-01-02 | Electrical powers, comic fan |
  | Kirsten | 1985-02-01 | Super-strength, bullet-proof, short |
  | Mark | ? | Dragon |
  | Connie | 1983-03-03 | Psychic sword, Olympic fencer |
";
                expect(&table) to make(format!("{}", table)) equal(expected)
            }

            when indent_3_table {
                let expected = String::from(
r"   | Name    | Date       | Notes                               |
   | ------- | ---------- | ----------------------------------- |
   | David   | 1982-01-02 | Electrical powers, comic fan        |
   | Kirsten | 1985-02-01 | Super-strength, bullet-proof, short |
   | Mark    | ?          | Dragon                              |
   | Connie  | 1983-03-03 | Psychic sword, Olympic fencer       |
");
                let content = 
r"   | Name | Date | Notes |
   | --- | --- | --- |
   | David | 1982-01-02 | Electrical powers, comic fan |
   | Kirsten | 1985-02-01 | Super-strength, bullet-proof, short |
   | Mark | ? | Dragon |
   | Connie | 1983-03-03 | Psychic sword, Olympic fencer |
";
                expect(&table) to make(format!("{}", table)) equal(expected)
            }

            when alignment_table {
                let expected = String::from(
r"| Name    | Count | Flight | Strong | Other                                   |
| ------- | ----: | :----: | :----: | :-------------------------------------- |
| David   |   134 |   Y    |   N    | Many uses of electrical powers, genius  |
| Kirsten |     9 |   N    |   Y    | Not sure of upper limit of her strength |
| Mark    | 3,000 |   Y    |   Y    | In dragon form he is extremely strong   |
| Connie  |    27 |   N    |   N    | Psychic sword is stunning weapon        |
");
                let content = 
r"| Name | Count | Flight | Strong | Other |
| --- | --: | :-: | :-: | :-- |
| David | 134 | Y | N | Many uses of electrical powers, genius |
| Kirsten | 9 | N | Y | Not sure of upper limit of her strength |
| Mark | 3,000 | Y | Y | In dragon form he is extremely strong |
| Connie | 27 | N | N | Psychic sword is stunning weapon |
";
                expect(&table) to make(format!("{}", table)) equal(expected)
            }

            when alignment_2_table {
                let expected = String::from(
r"|  Name   | Count | Flight | Strong | Other                                   |
| :-----: | ----: | :----: | :----: | --------------------------------------- |
|  David  |   134 |   Y    |   N    | Many uses of electrical powers, genius  |
| Kirsten |     9 |   N    |   Y    | Not sure of upper limit of her strength |
|  Mark   | 3,000 |   Y    |   Y    | In dragon form he is extremely strong   |
| Connie  |    27 |   N    |   N    | Psychic sword is stunning weapon        |
");
                let content = 
r"| Name | Count | Flight | Strong | Other |
| :--: | --: | :-: | :-: | --- |
| David | 134 | Y | N | Many uses of electrical powers, genius |
| Kirsten | 9 | N | Y | Not sure of upper limit of her strength |
| Mark | 3,000 | Y | Y | In dragon form he is extremely strong |
| Connie | 27 | N | N | Psychic sword is stunning weapon |
";
                expect(&table) to make(format!("{}", table)) equal(expected)
            }

            when short_row_table {
                let expected = String::from(
r"| Name    | Count | Flight | Strong | Other                                   | Genius |
| ------- | ----: | :----: | :----: | :-------------------------------------- | :----: |
| David   |   134 |   Y    |   N    | Many uses of electrical powers, genius  |   Y    |
| Kirsten |     9 |   N    |   Y    | Not sure of upper limit of her strength |        |
| Mark    | 3,000 |   Y    |   Y    | In dragon form he is extremely strong   |        |
| Connie  |    27 |   N    |   N    | Psychic sword is stunning weapon        |        |
");
                let content = 
r"| Name | Count | Flight | Strong | Other | Genius |
| --- | --: | :-: | :-: | :-- | :-: |
| David | 134 | Y | N | Many uses of electrical powers, genius | Y |
| Kirsten | 9 | N | Y | Not sure of upper limit of her strength |
| Mark | 3,000 | Y | Y | In dragon form he is extremely strong |
| Connie | 27 | N | N | Psychic sword is stunning weapon |
";
                expect(&table) to make(format!("{}", table)) equal(expected)
            }

            when short_header_table {
                let expected = String::from(
r"| Name    | Count | Flight | Strong | Other                                   |     |
| ------- | ----: | :----: | :----: | :-------------------------------------- | :-: |
| David   |   134 |   Y    |   N    | Many uses of electrical powers, genius  |  Y  |
| Kirsten |     9 |   N    |   Y    | Not sure of upper limit of her strength |  N  |
| Mark    | 3,000 |   Y    |   Y    | In dragon form he is extremely strong   |  N  |
| Connie  |    27 |   N    |   N    | Psychic sword is stunning weapon        |  N  |
");
                let content = 
r"| Name | Count | Flight | Strong | Other |
| --- | --: | :-: | :-: | :-- | :-: |
| David | 134 | Y | N | Many uses of electrical powers, genius | Y |
| Kirsten | 9 | N | Y | Not sure of upper limit of her strength | N |
| Mark | 3,000 | Y | Y | In dragon form he is extremely strong | N |
| Connie | 27 | N | N | Psychic sword is stunning weapon | N |
";
                expect(&table) to make(format!("{}", table)) equal(expected)
            }

            when long_row_table {
                let expected = String::from(
r"| Name    | Count | Flight | Strong | Other                                   |        |
| ------- | ----: | :----: | :----: | :-------------------------------------- | ------ |
| David   |   134 |   Y    |   N    | Many uses of electrical powers, genius  |        |
| Kirsten |     9 |   N    |   Y    | Not sure of upper limit of her strength |        |
| Mark    | 3,000 |   Y    |   Y    | In dragon form he is extremely strong   | Dragon |
| Connie  |    27 |   N    |   N    | Psychic sword is stunning weapon        |        |
");
                let content = 
r"| Name | Count | Flight | Strong | Other |
| --- | --: | :-: | :-: | :-- |
| David | 134 | Y | N | Many uses of electrical powers, genius |
| Kirsten | 9 | N | Y | Not sure of upper limit of her strength |
| Mark | 3,000 | Y | Y | In dragon form he is extremely strong | Dragon |
| Connie | 27 | N | N | Psychic sword is stunning weapon |
";
                expect(&table) to make(format!("{}", table)) equal(expected)
            }
        }
    }
}
