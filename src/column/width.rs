//! Struct representing the width of a column.
//!
//! # Examples
//!
//! ```rust
//! use mdtable::ColWidth;
//!
//! # fn main() {
//! let width = ColWidth::new(20);
//! assert!(20usize == width.get());
//!
//! // Cannot create below 3
//! let width = ColWidth::new(2);
//! assert!(3usize == width.get());
//!
//! // Cannot create above 200
//! let width = ColWidth::new(20_000);
//! assert!(200usize == width.get());
//! # }
//! ```
//!
//! # Description
//!
//! Limited size for columns. A column can be no smaller that 3 and no greater than 200.

use std::cmp;

/// New-type representing a column width
#[derive(Debug, Clone, Copy, Eq, Ord, PartialEq, PartialOrd)]
#[allow(clippy::module_name_repetitions)]
pub struct ColWidth(u16);

// Minimum and maximum column sizes.
const MIN_WIDTH: usize = 3;
const MAX_WIDTH: usize = 200;

/// Default size is the minimum size.
impl Default for ColWidth {
    #[allow(clippy::cast_possible_truncation)]
    fn default() -> Self { Self(MIN_WIDTH as u16) }
}

impl ColWidth {
    /// Constructor for [`ColWidth`]
    ///
    /// Clamps the supplied size to the range defined, instead of reporting an error.
    #[must_use]
    pub fn new(width: usize) -> Self {
        #[allow(clippy::cast_possible_truncation)]
        Self(width.clamp(MIN_WIDTH, MAX_WIDTH) as u16)
    }

    /// Retrieve the internal value as a `usize`.
    #[must_use]
    pub const fn get(&self) -> usize { self.0 as usize }
}

impl From<i32> for ColWidth {
    #[allow(clippy::cast_sign_loss)]
    fn from(w: i32) -> Self { Self::new(cmp::max(w, 0) as usize) }
}

impl From<u32> for ColWidth {
    fn from(w: u32) -> Self { Self::new(w as usize) }
}

impl From<u16> for ColWidth {
    fn from(w: u16) -> Self { Self::new(w as usize) }
}

impl From<usize> for ColWidth {
    fn from(w: usize) -> Self { Self::new(w) }
}

#[cfg(test)]
mod tests {
    use super::*;

    use lets_expect::lets_expect;

    lets_expect! {
        expect(ColWidth::default()) to equal(ColWidth::new(3))
        expect(ColWidth::new(0)) as clamped_to_min to have(get()) equal(MIN_WIDTH as usize)
        expect(ColWidth::new(10_000)) as clamped_to_max to have(get()) equal(MAX_WIDTH as usize)
        expect(ColWidth::new(10)) as new to have(get()) equal(10)
    }
}
