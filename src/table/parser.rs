//! Define a parser for extracting a table from a markdown description.
use regex::Regex;
use once_cell::sync::Lazy;

use crate::{Alignment, Row, Table};

static SEP_LINE_RE: Lazy<Regex> =
    Lazy::new(|| Regex::new(r"\A {0,3}\|(?: *[-:]-+[-:] *\|)+ *\z").expect("Separator line"));

static TABLE_LINE_RE: Lazy<Regex> =
    Lazy::new(|| Regex::new(r"\A( {0,3})\|(?: *[^|]* *\|)+ *\z").expect("Table line"));

/// Errors that can be produced by this struct. Mostly covers potential parsing errors.
#[derive(Debug, thiserror::Error, PartialEq, Eq)]
pub enum Error {
    /// Table not found before other content.
    #[error("Table not found")]
    TableNotFound,

    /// No header line was found before the separator.
    #[error("No header found before separator")]
    MissingHeader,

    /// No separator line was found after the header.
    #[error("No separator found after header")]
    MissingSeparator,

    /// More than one separator line
    #[error("More than one separator line found in table")]
    ExtraSeparator,

    /// Extraneous content after the table.
    #[error("Content found after table")]
    ContentAfterTable,
}

/// Enumeration of the different types of lines encountered during a parse.
pub enum LineType<'a> {
    NonTableLine(&'a str),
    TableLine(usize, Row),
    Separator(Vec<Alignment>),
}

/// Structure representing the parsing of a table
#[derive(Debug, Default)]
#[allow(clippy::module_name_repetitions)]
pub struct TableParser {
    table: Table,
    state: TableState
}

impl TableParser {
    /// Parse a table out of the supplied string.
    ///
    /// # Errors
    ///
    /// - Return an [`Error::TableNotFound`] if table not at beginning of string
    /// - Return an [`Error::ContentAfterTable`] if non-blank line after table
    /// - Return an [`Error::MissingSeparator`] if no separator line after header
    pub fn parse_str(mut self, s: &str) -> Result<Table, Error> {
        for l in s.lines() {
            match (self.add_line(l)?, l.trim()) {
                (AddStatus::Added, _) |
                (AddStatus::NotStarted, "") => (),
                (AddStatus::NotStarted, _) => return Err(Error::TableNotFound),
                (AddStatus::Finish, "") => break,
                (AddStatus::Finish, _) => return Err(Error::ContentAfterTable),
            }
        }
        match self.state {
            TableState::Header => Err(Error::TableNotFound),
            TableState::SepLine => Err(Error::MissingSeparator),
            TableState::Row => Ok(self.table)
        }
    }

    /// Return the parsed [`Table`] if successful
    ///
    /// # Errors
    ///
    /// - Return an [`Error::TableNotFound`] if no table found
    /// - Return an [`Error::MissingSeparator`] if no separator line after header
    pub fn finish(self) -> Result<Table, Error> {
        match self.state {
            TableState::Header => Err(Error::TableNotFound),
            TableState::SepLine => Err(Error::MissingSeparator),
            TableState::Row => Ok(self.table)
        }
    }

    /// Interpret a line and add it to the table if appropriate
    ///
    /// # Errors
    ///
    /// - Return an [`Error::TableNotFound`] if no table found
    /// - Return an [`Error::MissingHeader`] if no header line at beginning of table
    /// - Return an [`Error::MissingSeparator`] if no separator line after header
    /// - Return an [`Error::ExtraSeparator`] if more than one separator line found
    pub fn add_line(&mut self, line: &str) -> Result<AddStatus, Error> {
        match (parse_line(line), self.state) {
            (LineType::NonTableLine(_), TableState::Header) => Ok(AddStatus::NotStarted),
            (LineType::NonTableLine(_) | LineType::TableLine(..), TableState::SepLine)
                => Err(Error::MissingSeparator),
            (LineType::NonTableLine(_), TableState::Row) => Ok(AddStatus::Finish),

            (LineType::TableLine(indent, row), TableState::Header) => {
                self.table.add_heading(row, indent);
                self.state = self.state.next();
                Ok(AddStatus::Added)
            },
            (LineType::TableLine(_, row), TableState::Row) => {
                self.table.add_row(row);
                self.state = self.state.next();
                Ok(AddStatus::Added)
            },

            (LineType::Separator(_), TableState::Header) => Err(Error::MissingHeader),
            (LineType::Separator(sep), TableState::SepLine) => {
                self.table.add_separator(sep);
                self.state = self.state.next();
                Ok(AddStatus::Added)
            },
            (LineType::Separator(_), TableState::Row) => Err(Error::ExtraSeparator),
        }
    }
}

/// State of attempted parsing of a table.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum AddStatus {
    NotStarted,
    Added,
    Finish
}

// State of the construction of a table
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq)]
enum TableState {
    #[default]
    Header,
    SepLine,
    Row
}

impl TableState {
    const fn next(self) -> Self {
        match self {
            Self::Header => Self::SepLine,
            Self::SepLine | Self::Row => Self::Row,
        }
    }
}

// Extract the content of the cells of a row from line
fn extract_cells(line: &str) -> impl Iterator<Item=&str> {
    let line = line.trim().strip_prefix('|').unwrap_or(line);
    let line = line.strip_suffix('|').unwrap_or(line);

    line.split('|').map(str::trim)
}

/// Parse a line into the appropriate type if possible
pub fn parse_line(s: &str) -> LineType {
    if s.trim() == "" { return LineType::NonTableLine(s); }

    if SEP_LINE_RE.is_match(s) {
        let sep: Vec<Alignment> = extract_cells(s).map(|s| Alignment::new(s, s.len())).collect();
        return LineType::Separator(sep);
    }

    if let Some(caps) = TABLE_LINE_RE.captures(s) {
        let index = caps[1].len();
        let cells: Vec<String> = extract_cells(s).map(String::from).collect();
        return LineType::TableLine(index, Row::new(cells));
    }

    LineType::NonTableLine(s)
}
