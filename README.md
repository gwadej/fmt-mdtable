# README

I was working on a project at work that needed some note information in a table format. Since I was
used to working in Markdown, I used it for these notes. The annoying part was trying to keep the
table from getting too unruly as I worked. I don't mind having the table a little out of line, but
adding columns and reformatting was getting annoying.

I wrote a quick Perl script to do some reformatting, and then decided to redo the script as a Rust
program as an exercise. This is the result.

## Execution

The current version expects the input on `stdin` and writes the output to `stdout`. Since I spend
most of my time in `vim`, this allows for a fairly comfortable use case. I can just execute
`:%!fmt-mdtable` at any time to format all of the tables in the markdown document I am currently
working on.
